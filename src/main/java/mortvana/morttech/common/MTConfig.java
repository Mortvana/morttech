package mortvana.morttech.common;

import java.io.File;

import net.minecraftforge.common.config.Configuration;
import org.apache.logging.log4j.Level;

public class MTConfig {

    public static void setup(String dir) {
        initMachines(new Configuration(new File(dir, "Machines")));
        //initWorld(new Configuration(new File(dir, "World")));
    }

    private static void initMachines(Configuration config) {
        try{
            config.load();
        } catch (Exception e) {
            //LogHandler.log(Level.ERROR, "There was an issue with when adjusting machine settings");
            e.printStackTrace();
        } finally {
            config.save();
        }
    }

}
