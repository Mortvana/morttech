package mortvana.morttech.common;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import mantle.lib.TabTools;
import net.minecraftforge.common.config.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Mod(modid="MortTech", name="MortTech", version="0.0.0.2", dependencies = "required-after:Forge@[9.11,); required-after:Mantle")
public class MortTech {
    public static final Logger logger = LogManager.getLogger("MortTech");
	@Instance("MortTech")
    public static MortTech INSTANCE;
    @SidedProxy(clientSide = "mortvana.morttech.client.ClientProxy", serverSide = "mortvana.morttech.common.CommonProxy")
    public static CommonProxy proxy;

    public MortTech(){
        EnvironmentChecks.verifyEnvironmentSanity();
    }

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {

        //root = event.getModConfigurationDirectory();
        //Config.setup(root + "/MortTech/");
        //for(Module module: Modules.modules) {
        //    module.preInit();
        //}

    	MTContent.componentsTab = new TabTools("MTComponents");
        MTContent.toolsTab = new TabTools("MTTools");
        MTContent.machineTab = new TabTools("MTMachines");
    	
        MTContent.preInit();

    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {

    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }
}
