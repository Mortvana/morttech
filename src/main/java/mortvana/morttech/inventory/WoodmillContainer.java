package mortvana.morttech.inventory;

import mortvana.morttech.recipes.WoodmillRecipes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;
import mortvana.morttech.block.machine.logic.WoodmillLogic;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class WoodmillContainer extends Container{
    private WoodmillLogic woodmill;
    private int progressTime;
    private int powerTime;
    private int powerGaugeTime;

    public WoodmillContainer(InventoryPlayer InventoryPlayer, WoodmillLogic TileEntityWoodmill)
    {
        this.woodmill = TileEntityWoodmill;
        this.addSlotToContainer(new Slot(TileEntityWoodmill, 0, 56, 17));
        this.addSlotToContainer(new Slot(TileEntityWoodmill, 1, 56, 53));
        this.addSlotToContainer(new SlotFurnace(InventoryPlayer.player, TileEntityWoodmill, 2, 116, 35));
        int i;

        for (i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(InventoryPlayer, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(InventoryPlayer, i, 8 + i * 18, 142));
        }
    }

    @Override
    public void addCraftingToCrafters (ICrafting par1ICrafting)
    {
        super.addCraftingToCrafters(par1ICrafting);
        par1ICrafting.sendProgressBarUpdate(this, 0, this.woodmill.progress);
        par1ICrafting.sendProgressBarUpdate(this, 1, this.woodmill.power);
        par1ICrafting.sendProgressBarUpdate(this, 2, this.woodmill.powerGauge);
    }

    /**
     * Looks for changes made in the container, sends them to every listener.
     */
    @Override
    public void detectAndSendChanges ()
    {
        super.detectAndSendChanges();

        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting) this.crafters.get(i);

            if (this.progressTime != this.woodmill.progress)
            {
                icrafting.sendProgressBarUpdate(this, 0, this.woodmill.progress);
            }

            if (this.powerTime != this.woodmill.power)
            {
                icrafting.sendProgressBarUpdate(this, 1, this.woodmill.power);
            }

            if (this.powerGaugeTime != this.woodmill.powerGauge)
            {
                icrafting.sendProgressBarUpdate(this, 2, this.woodmill.powerGauge);
            }
        }

        this.progressTime = this.woodmill.progress;
        this.powerTime = this.woodmill.power;
        this.powerGaugeTime = this.woodmill.powerGauge;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void updateProgressBar (int par1, int par2)
    {
        if (par1 == 0)
        {
            this.woodmill.progress = par2;
        }

        if (par1 == 1)
        {
            this.woodmill.power = par2;
        }

        if (par1 == 2)
        {
            this.woodmill.powerGauge = par2;
        }
    }

    @Override
    public boolean canInteractWith (EntityPlayer par1EntityPlayer)
    {
        return this.woodmill.isUseableByPlayer(par1EntityPlayer);
    }

    /**
     * Called when a player shift-clicks on a slot. You must override this or
     * you will crash when someone does that.
     */
    @Override
    public ItemStack transferStackInSlot (EntityPlayer par1EntityPlayer, int par2)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot) this.inventorySlots.get(par2);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (par2 == 2)
            {
                if (!this.mergeItemStack(itemstack1, 3, 39, true))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }
            else if (par2 != 1 && par2 != 0)
            {
                if (WoodmillRecipes.cutting().getCuttingResult(itemstack1) != null)
                {
                    if (!this.mergeItemStack(itemstack1, 0, 1, false))
                    {
                        return null;
                    }
                }
                else if (TileEntityFurnace.isItemFuel(itemstack1))
                {
                    if (!this.mergeItemStack(itemstack1, 1, 2, false))
                    {
                        return null;
                    }
                }
                else if (par2 >= 3 && par2 < 30)
                {
                    if (!this.mergeItemStack(itemstack1, 30, 39, false))
                    {
                        return null;
                    }
                }
                else if (par2 >= 30 && par2 < 39 && !this.mergeItemStack(itemstack1, 3, 30, false))
                {
                    return null;
                }
            }
            else if (!this.mergeItemStack(itemstack1, 3, 39, false))
            {
                return null;
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack) null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
        }

        return itemstack;
    }
}

	
