package mortvana.morttech.block.machine;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import mantle.blocks.MantleBlock;
import mortvana.morttech.block.machine.logic.WoodmillLogic;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.List;

public class BlockCrank extends MantleBlock {

	public BlockCrank(Material material) {
    	super(material);
    }

    //TODO
    //Fixing...
    /*@Override
    public boolean onBlockActivated(World world, int xCoord, int yCoord, int zCoord, EntityPlayer entPlayer) {

        WoodmillLogic tile = (BlockMachine)World.getBlockTileEntity(world, xCoord, yCoord - 1, zCoord, entPlayer);
        if (tile != null) {
            tile.power += 10;
        }

        return true;
    }*/

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean isBlockSolid(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5) {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public int getRenderType() {
        return -1;
    }

   @SideOnly(Side.CLIENT)
    public void addInformation (ItemStack stack, EntityPlayer player, List list, boolean par4)
    {
        switch (stack.getItemDamage())
        {
        case 0:
            list.add("A simple, but useful wooden crank.");
            list.add("Used to power most simple machinery.");
            break;

        }
    }
    
}
